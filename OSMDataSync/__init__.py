# -*- coding: utf-8 -*-
"""
/***************************************************************************
 OSMDataSync
                                 A QGIS plugin
 This Plugin allows the user to traverse and sync features from OSM.
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                             -------------------
        begin                : 2020-06-25
        copyright            : (C) 2020 by Christopher Hilfing
        email                : chilfing@hsr.ch
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""

import os
import sys

sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), "dependencies"))
)


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load OSMDataSync class from file OSMDataSync.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    from .osm_data_sync import OSMDataSync

    return OSMDataSync(iface)
